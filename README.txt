RunningLogDownloader

This is a Python tool for downloading all your logs on running-log.com.

Requirements:
1. Have python3 installed on your machine. You can do that here: https://www.python.org/downloads/
2. Install BeautifulSoup. This  can be done by running the command "pip install BeautifulSoup4" on your command line.
3. Install requests. This  can be done by running the command "pip install requests" on your command line.
4. Have an account on running-log.com

How to use:
First download the the file in this repository called "running_log_downloader.py" into the directory where you want all your logs saved. Browse to the directory and open the settings.json file. Replace "your-username" and "your-password" with your email and password credentials for running-log.com, such as "eskeddit@lilpump.com" and "hunter2". Then, open a terminal in this directory and run the command "python running_log_downloader.py".

This script will go back in your log history and download every log that you have posted - starting with your oldest ones - as html files. You will see in a file explorer that two folders will have been made in the directory where you saved this script: "logs" and "assets". "logs" is the folder where all your logs are to be saved, and "assets" just contains the running-log logo and a css file. The assets are included to make the pages that are downloaded look a little nicer.


File names for your logs follow the following format: DATE(Time of Day)_(Exercise Type)_TITLE.html. Example: "2017-03-05(M)_(Run)_Attempt to run through injury.html". The date is in the form YYYY-MM-DD, and the Time of Day is either M (morning), A (afternoon),  or N (Night). Addtionally, all invalid file name characters are removed from log titles.

Upon each run of the script, files will be overwritten. None will be deleted.


OTHER SETTINGS
In the settings.json file there are also other settings that can be changed.

failover-interval: running-log.com is known to go down frequently and having retries can be helpful. This number specifies how many seconds this script waits before trying again if running-log is down. So if running-log goes down while the script is downloading the webpages, the script will repeatedly try whatever process it was on every 5 seconds (or however many seconds you specified).

download-only-most-recent: this value can be set to either true or false. If true, only the most recent logs (the first page) will be downloaded. If false, then your entire log history will be downloaded.

output: this is the output format of the logs. There are three options: HTML, TEXT, or BOTH.
	HTML: The default option. Each log is downloaded as its own html file.
	TEXT: Each log is compiled into one large text file. 
	BOTH: Both types of outputs will be created. When the text file is finished, it will be created as "all_my_logs.txt" in the executing folder of this program.



MAC USERS/PPL WHO HAVE NEVER USED PYTHON BEFORE/DON'T KNOW WHAT A TERMINAL IS:

Okay, buckle up. If you have no previous python experience and also use a Mac, then welcome to the tutorial for you.

First off, download python in step 1. Make sure that you choose the version that starts with 3. After that, you're going to run into some issues. 

The command line that Jacob mentions is called "terminal" on Mac. Open spotlight(either hit CMD+Space, or click the magnifying glass in the upper right hand corner of the menu bar) and launch it. 

Now, for the second step that Jacob put down, copy and paste this(WITHOUT the quotation marks, obviously). "python3 -m pip install BeautifulSoup4". This should trigger a bunch of stuff to happen that frankly you don't care about. Congrats.

For the next step, keep terminal open, but this time type, "python3 -m pip install requests". Again, you should be seeing a bunch of stuff loading on the screen.

Alright, now we're going to settings. Go to Keyboard -> shortcuts -> services(found on the left side menu), and then scroll down on the right and check the box next to, "New Terminal at Folder". 

At this point you need to move the file called "running_log_downloader.py" that you downloaded from Jacob to wherever you want the logs to be. Make sure it's in a folder. 

Now, right click on the folder with the file in it. At the bottom click, "New Terminal at Folder". This will in turn open a terminal window(duh). Now type, "python3 running_log_downloader.py".
Congrats. You're done. The logs that you just downloaded will be in the folder and you're all set. 

If that didn't work, hit me up on twitter @alanglinais7 and I might help you if you have enough clout.





