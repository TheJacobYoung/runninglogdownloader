# external libraries:
# pip install BeautifulSoup4
# pip install requests
import requests
from bs4 import BeautifulSoup
import sys
import re
import os
import os.path
import time
from time import strptime
import json

# constants
RUNNING_LOG_URL = 'http://www.running-log.com'
RUNNING_LOG_LOGIN_URL = RUNNING_LOG_URL + "/athlete/login"
RUNNING_LOG_WORKOUTS_URL = RUNNING_LOG_URL + "/workouts"
RUNNING_LOG_WORKOUT_PAGE_URL = RUNNING_LOG_WORKOUTS_URL + "?page="

USERNAME_FORM_NAME = 'athlete[login]'
PASSWORD_FORM_NAME = 'athlete[password]'
ASSET_DIR_NAME = "assets" # folder for images and css
LOG_DIR_NAME = "logs" # folder where all the logs will be downloaded to

CSS_FILE_NAME = 'application.css'
CSS_PATH = ASSET_DIR_NAME + "/" + CSS_FILE_NAME

LOGO_FILE_NAME = 'logo_small.png'
LOGO_PATH = ASSET_DIR_NAME + "/" + LOGO_FILE_NAME

OUTPUTS = ["HTML","TEXT","BOTH"]

SPACING = 20 # the number dashes between each category (distance, duration, pace...)


def login(session, email, password):

    # This is the form data in the login screen that the page sends when logging in
    login_data = {
        USERNAME_FORM_NAME: email,
        PASSWORD_FORM_NAME: password,
    }

    # send the login request
    request_result = session.post(RUNNING_LOG_LOGIN_URL, data=login_data)
    
    if request_result.status_code != 200:
        print ("Something went wrong. The page you are trying to access ({}) might be down. Try again later.".format(RUNNING_LOG_LOGIN_URL))
        print ("Error code: {}".format(request_result.status_code))
        exit(1)

def get_num_pages(session, download_only_most_recent):
    # find out how many workoutpages there are

    if download_only_most_recent:
        return 1

    request_result = session.get(RUNNING_LOG_WORKOUTS_URL)

    if request_result.status_code != 200:
        print ("Something went wrong. The page you are trying to access ({}) might be down. Try again later.".format(RUNNING_LOG_WORKOUTS_URL))
        print ("Error code: {}".format(request_result.status_code))
        exit(1)

    # get number of pages
    workouts_page = BeautifulSoup(request_result.text, 'html.parser')
    workout_list = workouts_page.find(id="workout_list")

    if workout_list == None:
        print("Invalid login. Check the credentials provided in settings.json.")
        exit(1)

    pagination = workout_list.find("div", {"class": "pagination"})

    if pagination == None:
        num_pages = 1
    else:
        a_tags = pagination.find_all("a")
        num_pages = int(a_tags[len(a_tags)-2].string)

    return num_pages

def make_directories():
    # make directories for files on the user's drive if they don't exist

    if not os.path.exists(ASSET_DIR_NAME):
        os.makedirs(ASSET_DIR_NAME)

    if not os.path.exists(LOG_DIR_NAME):
        os.makedirs(LOG_DIR_NAME)

def get_links_to_workouts(workout_list):
    # get all workouts on the page
    table_rows = workout_list.find_all("tr", {'class':['row_odd', 'row_even']})
    links = []
    for row in table_rows:
        # get first a tag
        link = row.find('a')['href']
        links.append(link)
    return links

def configure_css(session, workout_page, failover_interval):

    def find_css_link(workout_page, try_num):
        print("Finding css_link: try {}".format(try_num))
        css_link = workout_page.find("link", {'rel': "stylesheet"})["href"] # find link to the css in the page
        if css_link == None: # could not find css link. Something went wrong.
            print("Oops, something went wrong finding the css link. The site might be down.")
            return None
        workout_page.find("link", {'rel': "stylesheet"})["href"] = "../" + ASSET_DIR_NAME + "/" + css_link[13:] # change path of the file
        return css_link

    def download_css_file(session, css_link, try_num):
        print("downloading css file: try {}...".format(try_num))
        css_request_result = session.get(RUNNING_LOG_URL + '/' + css_link)
        if css_request_result.status_code == 200:
            open(CSS_PATH, 'wb').write(css_request_result.content)
            print("SUCCESS: css file downloaded")
        else:
            print("ERROR: Failed to download running-log css file. Error code: {}".format(css_request_result.status_code))
        return css_request_result.status_code


    css_link = None
    try_num = 1
    while css_link == None:
        css_link = find_css_link(workout_page, try_num)
        if css_link == None:
            print("Finding css_link: Trying again in {} seconds...".format(failover_interval))
            time.sleep(failover_interval)
            try_num += 1

    # only try to download css if we don't already have it saved on drive
    if not os.path.isfile(CSS_PATH):
        download_status = None
        try_num = 1
        while download_status != 200:
            download_status = download_css_file(session, css_link, try_num)
            if download_status != 200:
                print("downloading css file: Trying again in {} seconds...".format(failover_interval))
                time.sleep(failover_interval)
                try_num += 1


def configure_running_log_logo(session, workout_page, failover_interval):

    def find_running_log_logo_link(workout_page, try_num):
        print("Finding running_log_logo: try {}".format(try_num))
        logo_link = workout_page.find("div", {'id': "main_logo"}).a.img["src"] # find link to the logo image in the page
        if logo_link == None:
            print("Oops, something went wrong finding the running log logo image. the site might be down.")
            return None
        workout_page.find("div", {'id': "main_logo"}).a.img["src"] = "../" + ASSET_DIR_NAME + "/" + logo_link[8:] # change path of the file to be local
        return logo_link
                
    def download_running_log_logo_file(session, logo_link, try_num):
        print("downloading logo file (try {})...".format(try_num))
        logo_request_result = session.get(RUNNING_LOG_URL + '/' + logo_link)
        if logo_request_result.status_code == 200:
            with open(LOGO_PATH, 'wb') as f:
                for chunk in logo_request_result.iter_content():
                    f.write(chunk)
            print("SUCCESS: running log logo file downloaded")
        else:
            print("ERROR: Failed to download running-log logo file. Error code: {}".format(logo_request_result.status_code))
        return logo_request_result.status_code


    logo_link = None
    try_num = 1
    while logo_link == None:
        logo_link = find_running_log_logo_link(workout_page, try_num)
        if logo_link == None:
            print("finding running_log_logo: Trying again in {} seconds...".format(failover_interval))
            time.sleep(failover_interval)
            try_num += 1

    # only try to download running log logo if we don't already have it saved on drive
    if not os.path.isfile(LOGO_PATH):
        download_status = None
        try_num = 1
        while download_status != 200:
            download_status = download_running_log_logo_file(session, logo_link, try_num)
            if download_status != 200:
                print("download_running_log_logo: Trying again in {} seconds...".format(failover_interval))
                time.sleep(failover_interval)
                try_num += 1



def construct_log_file_name(workout_page):

    def convert_date(date_str):
         # convert the date from something like "May 26, 2018 (Afternoon)" to "2018-05-26(A)"
        date = date_str.split() # [month, day,, year, (time of day)]
        date[0] = date[0][0:3] # abbreviate month
        month_num = strptime(date[0],'%b').tm_mon
        if month_num < 10:
            month_num = "0" + str(month_num)
        day_num = date[1][:-1]
        if int(day_num) < 10:
            day_num = "0" + str(day_num)
        year = date[2]

        # add on the first letter for time of day
        time_of_day = date[3][0:2] + ")"
        date = "{}-{}-{}{}".format(year, month_num, day_num, time_of_day)
        return date

    # find spot in the page where the log title is
    interior_wrapper = workout_page.find("div", {'class': 'interior_wrapper'})
    if interior_wrapper == None:
        print("Oops, something went wrong. {} must be down. Try again later.".format(RUNNING_LOG_URL))
        exit(1)
    log_title_spot = interior_wrapper.find("h3")
    if log_title_spot == None:
        print("Oops, something went wrong. {} must be down. Try again later.".format(RUNNING_LOG_URL))
        exit(1)
    log_title = log_title_spot.string.strip()
    
    # find the date and the exercise type
    date_str = log_title_spot.next_sibling.next_sibling
    exercise_type_str = log_title_spot.next_sibling.next_sibling.next_sibling.next_sibling
    
    # strip of white space on the ends
    date_str =  date_str.string.strip()
    exercise_type_str = exercise_type_str.string.strip()

    
    date = convert_date(date_str)


    # parse the exercise type
    exercise_type_colon_index = exercise_type_str.find(':')
    exercise_type = exercise_type_str[exercise_type_colon_index+2:]

    # remove invalid file name characters
    title = re.sub(r'[\\/*?:"<>|]',"", log_title) # remove invalid characters for filename

    # combine each element into example: "2018-05-26(A)_Run_My dank title.html"
    file_name = "{}_({})_{}.html".format(date, exercise_type, title)

    return file_name

def clean_page(workout_page):
    # remove unneeded content
    # since these are static pages with links that don't work, we want to clear a lot of it up
    workout_page.find("div", {"class": "clearFix"}).extract() # remove header stuff (My Log, Teams, Store)
    workout_page.find("div", {"class": "interior_wrapper"}).find("h2").next_sibling.next_sibling.next_sibling.next_sibling.extract() # remove p tag for editing log
    workout_page.find("div", {"class": "interior_wrapper"}).find("h2").next_sibling.next_sibling.extract() # remove p tag for deleting log
    workout_page.find("div", {"class": "interior_wrapper"}).find("h2").extract() # remove View Workout

    # disable all links
    for a in workout_page.find_all("a"):
        a['href'] = "#"


def save_page(workout_page, file_name):
    # save the content of the page on your drive
    log_page = workout_page.prettify()
    path = LOG_DIR_NAME + "/" + file_name
    try:
        file = open(path,"w+") 
        file.write(log_page) 
        file.close()
    except Exception as e:
        print ('Failed to write file to ' + path)
        print ('Error: ' + str(e))
        exit(1)
    print("Downloaded: \"{}\"".format(file_name))

def get_list_of_workouts_on_page(session, page_num, failover_interval):
    url = RUNNING_LOG_WORKOUT_PAGE_URL + str(page_num)
    success = False
    try_num = 1
    while not success:
        # navigate to the workout page
        request_result = session.get(RUNNING_LOG_WORKOUT_PAGE_URL + str(page_num))
        if request_result.status_code != 200:
            print("Oops, something went wrong accessing {} (try {}). Error code: {}".format(url, try_num, request_result.status_code))
            print("{}: Trying again in {} seconds...".format(url, failover_interval))
            time.sleep(failover_interval)
            try_num += 1
        else:
            success = True

    workouts_page = BeautifulSoup(request_result.text, 'html.parser')
    workout_list = workouts_page.find(id="workout_list")
    if workout_list == None:
        print("Oops, something went wrong while finding the list of workouts. Perhaps your login credentials are invalid? Or try again later.")
        exit(1)
    return workout_list

def get_workout_page(session, link, failover_interval):
    # send request to server to get html file
    url = RUNNING_LOG_URL + link
    success = False
    try_num = 1
    while not success:
        request_result = session.get(RUNNING_LOG_URL + link)
        if request_result.status_code != 200:
            print("Oops, something went wrong downloading from {} (try {}). Error code: {}".format(url, try_num, request_result.status_code))
            print("{}: Trying again in {} seconds...".format(url, failover_interval))
            time.sleep(failover_interval)
            try_num += 1
        else:
            success = True

    # get the workout log page for parsing from BeautifulSoup
    workout_page = BeautifulSoup(request_result.text, 'html.parser')
    return workout_page

def build_comment_content(comments_tag):
    # running log does weird things with line breaks...
    for e in comments_tag.findAll('br'):
        e.replace_with("\n")

    comments = comments_tag.get_text().strip()

    # the followuing code was when I was parsing the downloaded html
    # newlines = comments.split("\n")

    # comments = ""
    # for token in newlines:
    #     if token == "":
    #         comments += "\n"
    #         continue
    #     token = token.strip()
    #     if token == "":
    #         continue
    #     comments += token + "\n"
    return comments + "\n\n"

def calculate_pace(distance, time):
    if time.count(':') == 2:
        hours, minutes, seconds = time.split(':')
    else:
        hours = 0
        minutes, seconds = time.split(':')

    total_seconds = (int(hours) * 3600) + (int(minutes) * 60) + int(seconds)
    seconds_per_mile = float(total_seconds) / float(distance)

    minutes_per_mile = int(seconds_per_mile / 60)
    seconds_remainder = int(seconds_per_mile - (minutes_per_mile * 60))

    return '{}:{:0=2d}'.format(minutes_per_mile, seconds_remainder)

def build_table_content(table_tag):

    thead_tag = table_tag.find_next()
    tr_thead = thead_tag.find_next()
    table = ""
    for thead in tr_thead.findAll("th"):
        table += thead.string.strip().ljust(SPACING)

    table += "\n"

    tbody_tag = table_tag.find("tbody")
    if tbody_tag == None:
        return table + "\n"

    for tr in tbody_tag.findAll("tr"):
        for td in tr.findAll("td"):
            if td.string == None:
                continue
            table += td.string.strip().ljust(SPACING)
        table += "\n"

    tfoot_tag = table_tag.find("tfoot")
    if tfoot_tag == None:
        return table

    table += "\nTOTALS:\n"
    tr_tag = tfoot_tag.find_next()

    total_distance_tag = tr_tag.find_next()
    total_distance = total_distance_tag.string.strip()
    table += total_distance.ljust(SPACING)

    total_time_tag = total_distance_tag.find_next()
    total_time = total_time_tag.string.strip()
    table += total_time.ljust(SPACING)

    total_distance = total_distance.split(" ") # remove "Miles"
    pace = calculate_pace(total_distance[0], total_time)
    pace += " / Mile"
    table += pace.ljust(SPACING)

    return table + "\n"

def build_weather_content(weather_tag):
    weather = weather_tag.string.strip()
    return weather + "\n"

def build_dashes():
    dashes=""
    for x in range(0,5): # 5 times to match the number of categories (distance, duration, pace...)
        for y in range (1, SPACING+1):
            dashes += "-"
    return dashes + "\n"

def add_log_to_text_file(all_logs_text_file, workout_page, log_number):
    log_content = "\n"
    log_content += build_dashes()
    log_content += "#" + str(log_number) + "\n"

    title_tag = workout_page.find("div", {"class": "interior_wrapper"}).find("h3")
    title = title_tag.string.strip()
    log_content += title+"\n"

    date_tag = title_tag.find_next()
    date = date_tag.string.strip()
    log_content += "Date: " + date + "\n"

    exercise_type_tag = date_tag.find_next()
    exercise_type = exercise_type_tag.string.strip()
    log_content += exercise_type+"\n"

    # next is either weather, comments, or table
    weather_or_comments_or_table_tag = exercise_type_tag.find_next()

    if weather_or_comments_or_table_tag.name == "table": # table tag is next
        table_content = build_table_content(weather_or_comments_or_table_tag)
        log_content += table_content

    elif weather_or_comments_or_table_tag.string == None: # comments tag is next
        comments = build_comment_content(weather_or_comments_or_table_tag)
        log_content += comments

        table_tag = weather_or_comments_or_table_tag.find_next()

        table_content = build_table_content(table_tag)
        log_content += table_content

    else: # weather tag is next
        weather_content = build_weather_content(weather_or_comments_or_table_tag)
        log_content += weather_content

        comments_or_table_tag = weather_or_comments_or_table_tag.find_next()

        if comments_or_table_tag.string == None: # comments tag is next
            comments = build_comment_content(comments_or_table_tag)
            log_content += comments
            comments_or_table_tag = comments_or_table_tag.find_next() # now table tag

        table_content = build_table_content(comments_or_table_tag)
        log_content += table_content

    log_content += build_dashes()

    log_content += "\n"

    all_logs_text_file.write(log_content)
    print("Added log " + title)


def main():

    failover_interval = 5 # how many seconds to wait until trying again upon failure
    download_only_most_recent = False; # if true, only downloads the most recent page of logs
    output = "HTML"

    # read settings from settings file if it exists
    try:
        settings = json.load(open('settings.json'))
        settings = settings['running-log']
        email = settings['email']
        password = settings['password']
        failover_interval = settings['failover-interval'] 
        download_only_most_recent = settings['download-only-most-recent']
        output = settings['output']
        if output not in OUTPUTS: # invalid output setting, defaulting to HTML
            output = "HTML"
    except Exception as e:
        print ('Failed to read settings file.')
        print ('Make sure that settings.json is included in the working directory of this script and is formatted properly.')
        print ('See the README in this repo for help.')
        print ('Error: ' + str(e))
        exit(1)

    # start timer. interesting to see how long the script runs.
    start_time = time.time()

    # Start a web session so we can stay logged in
    session = requests.Session()

    login(session, email, password)

    # find out how many pages of workouts there are
    num_pages = get_num_pages(session, download_only_most_recent)

    # make directories for better organization of the files
    make_directories()

    if (output == "TEXT") or (output == "BOTH"):
        all_logs_text_file= open("all_my_logs.txt","w")
        header = "MY RUNNING LOGS" + "\n"
        header += build_dashes()
        all_logs_text_file.write(header)

    # counter for num logs downloaded. for fun
    num_logs = 0
    num_pages_left = num_pages

    # process each workout page
    while num_pages_left > 0:

        workout_list = get_list_of_workouts_on_page(session, num_pages_left, failover_interval)

        log_links = get_links_to_workouts(workout_list)

        # reverse the links so that we start with the oldest log
        log_links = log_links[::-1]

        for link in log_links: # for each log

            # this is the page that has the content for a single log
            workout_page = get_workout_page(session, link, failover_interval)

            if (output == "BOTH") or (output  == "HTML"):
                # download the css if it is not already saved on your drive and modify the path in the html
                configure_css(session, workout_page, failover_interval)

                # download the running log logo if it is not already saved on your drive and modify the path in the html
                configure_running_log_logo(session, workout_page, failover_interval)

                # put together the name of the file to be saved on to the drive
                file_name = construct_log_file_name(workout_page)
                
                # remove unneeded elements on the log page
                clean_page(workout_page)

                # save the log page to the user's drive
                save_page(workout_page, file_name)
            
            num_logs+=1
            if (output == "BOTH") or (output == "TEXT"):
                add_log_to_text_file(all_logs_text_file, workout_page, num_logs)
        num_pages_left -= 1

    if all_logs_text_file:
        all_logs_text_file.close() 

    print()
    print("Num logs downloaded: {}".format(num_logs))
    print("Time elapsed: {} seconds".format(time.time() - start_time))

if __name__ == '__main__':
    main()
